﻿using System;

namespace ConsoleApp99
{
    class Program
    {
        static void Main(string[] args)
        {
           int[] arr = { 1000, 432, 54000, 34, 31, 876, 32, 867, 54 };
            int startIndex = 3;

            int index = startIndex;
            int max = arr[index];
            for (int i = startIndex + 1; i < arr.Length; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                    index = i;
                }
            }
            Console.WriteLine(index);
            Console.WriteLine(max);
            Console.ReadLine();

        }
    }
}
